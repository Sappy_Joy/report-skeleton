#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

int main(void)
{
    char buf[1024];

    int amount = 1;

    char* strings[10]={"a","da","asd","asdf","qwert","qwerty","qwertyu","cvbnm,./","123456789","1234567890"};

    int proc_fd = open("/proc/var1", O_RDONLY);
    int dev_fd = open("/dev/var1", O_WRONLY);

    if(dev_fd==-1){
        printf("Can't open /dev/var1. Try with sudo\n");
        return -1;
    }
    for(int i=0; i < amount; i++){
        lseek(dev_fd, 0 , SEEK_SET);
        if(write(dev_fd, strings[i], strlen(strings[i]))==-1){
            printf("write failed");
        };
    };

    lseek(proc_fd, 0 , SEEK_SET);
    read(proc_fd, buf, 1024);
    puts(buf);
}	