# Лабораторная работа 1
**№ варианта:** 1

**Название:** "Разработка драйверов символьных устройств"

**Цель работы:**
получить знания и навыки разработки драйверов символьных устройств для операционной системы Linux.

## Описание функциональности драйвера
Написать драйвер символьного устройства, удовлетворяющий требованиям:
1. Драйвер должен создавать символьное устройство `/dev/varN`, где `N` – это номер варианта.
2. Драйвер должен создавать интерфейс для получения сведений о результатах операций над созданным в п.1.1 символьным устройством: файл `/proc/varN`, где `N` – номер варианта.
3. Должен обрабатывать операции записи и чтения в соответствии с вариантом задания:
- При записи текста в файл символьного устройства должно запоминаться количество пробелов в тексте. Последовательность полученных результатов с момента загрузки модуля ядра должна выводиться при чтении созданного файла /proc/varN в консоль пользователя.
- При чтении из файла символьного устройства в кольцевой буфер ядра должен осуществляться вывод тех же данных, которые выводятся при чтении файла `/proc/varN`.

## Инструкция по сборке
### Сборка модуля
```
$ make
```
### Установка модуля
```
$ make install
```

### Удаление модуля
```
$ make remove
```
### Обновление модуля
```
$ sudo make update
```

## Инструкция пользователя
### Запись в символьное устройство
```
$ sudo sh -c "echo -n 'some random string' > /dev/var1"
```

### Получение количества символов записанных в устройство через  `/proc/var1`
```
$ sudo cat /proc/var1
```
### Изменение размера длины истории
В данной лабораторной работе был реализован механизм сохранения истории записи в файл, длина истории может быть сконфигурирована следующей командой
```
$ sudo sh -c 'echo <length> > /sys/module/iomodule/parameters/history_sz'
```