#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/kdev_t.h>

// #include <string.h>

#include "config.h"

#define LOG_MODULE(msg) "%s: module log - " msg "\n", THIS_MODULE->name
#define LOG_PROC(msg) "%s: proc log - " msg "\n", THIS_MODULE->name
#define LOG_DEV(msg) "%s: dev log - " msg "\n", THIS_MODULE->name

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Dzhukashev D R");

void push_to_history(int length);
int get_from_history(int position);
void make_zero_history(int position);

int elems_in_history = 0;
bool overflow = false;
int history_size = 5;
int history[HISTORY_MAX_SIZE];

static struct proc_dir_entry *ent;
static struct class *char_drv_class[ENTITIES];
static struct cdev char_dev;
static dev_t my_dev[ENTITIES];

void push_to_history(int length) {
    history[elems_in_history] = (length == 0) ? length - 1 : length;
    elems_in_history++;
    printk(KERN_INFO "history at %d position is %d\n", elems_in_history - 1, history[elems_in_history - 1]);
    if (elems_in_history == history_size) {
        elems_in_history = 0;
        overflow = true;
    }
}

void make_zero_history(int position) {
    history[position] = 0;
}

int get_from_history(int position) {
    if (!overflow) {
        return history[position];
    }
    return history[(elems_in_history + position) % history_size];
}

static int my_set(const char *val, const struct kernel_param *kp) {
    int n = 0, ret;
    ret = kstrtoint(val, 10, &n);
    if (ret = 0 || n < 1 || n > HISTORY_MAX_SIZE)
        return -EINVAL;

    int bufer[BUFSIZE];
    size_t i = 0;
    for (i; i < history_size; i++) {
        int history_value = get_from_history(i);
        if (!overflow && history_value == 0)
            break;
        bufer[i] = get_from_history(i);
        printk(KERN_INFO "Buffer at %d with value %d\n", i, bufer[i]);
    }

    printk(KERN_WARNING
    "there were %d elements in history", i);

    overflow = false;
    elems_in_history = 0;

    int prev_history_size = history_size;
    history_size = n;
    size_t j = 0;
    for (j; j < i; j++) {
        printk(KERN_INFO "updating history with value %d j position is %zu\n", bufer[j], j);
        push_to_history(bufer[j]);
    }
    for (i; i < history_size; i++) {
        make_zero_history(i);
    }
    return param_set_int(val, kp);
}

static const struct kernel_param_ops param_ops = {
        .set    = my_set,
        .get    = param_get_int,
};

module_param_cb(history_sz,&param_ops, &history_size, S_IRUGO|S_IWUSR);

static ssize_t proc_write(struct file *file, const char __user *ubuf, size_t count, loff_t *ppos){
    printk(KERN_INFO LOG_PROC("mock output while writing in proc"));
    return 0;
}

static ssize_t proc_read(struct file *file, char __user *ubuf, size_t count, loff_t *ppos){
    printk(KERN_ALERT LOG_PROC("read handler\n"));
    char buf[BUFSIZE];
    int len = 0;
    if(*ppos > 0 || count < BUFSIZE){
        printk(KERN_WARNING LOG_PROC("FAILED *ppos > 0 || count < BUFSIZE"));
        return 0;
    }
    int i = 0;
    for(i; i<history_size; i++){
        len += sprintf(buf+len,"%d\n",get_from_history(i));
    }
    printk(KERN_WARNING "Generated result \n %s", buf);
    if(copy_to_user(ubuf, buf, len)){
        printk(KERN_WARNING LOG_PROC("FAILED copy_to_user(ubuf,buf,len)"));
        return -EFAULT;
    }
    *ppos = len;
    return len;
}


static struct file_operations myops =
        {
                .owner = THIS_MODULE,
                .read = proc_read,
                .write = proc_write,
        };

static int dev_open(struct inode *i, struct file *file) {
    printk(KERN_INFO LOG_DEV("dev open"));
    return 0;
}

static int dev_close(struct inode *i, struct file *file) {
    printk(KERN_INFO LOG_DEV("dev close"));
    return 0;
}

static ssize_t dev_write(struct file *file, const char __user *ubuf, size_t count, loff_t *ppos) {
    printk(KERN_ALERT LOG_DEV("write handler\n"));
    char buf[BUFSIZE];
    if(*ppos > 0 || count > BUFSIZE){
        printk(KERN_ALERT LOG_DEV("failed *ppos > 0 || count > BUFSIZE"));
        return -EFAULT;
    }
    if(copy_from_user(buf, ubuf, count)){
        printk(KERN_ALERT LOG_DEV("FAILED copy_from_user(buf, ubuf, count)"));
        return -EFAULT;
    }
    push_to_history(count);
    printk(KERN_INFO "wrote %s in dev and it's length is %zu \n",buf,count);
    return count;
}

static ssize_t dev_read(struct file *file, char __user *ubuf, size_t count, loff_t* ppos) {
    printk(KERN_INFO LOG_DEV("dev read"));
    char buf[BUFSIZE];
    int len = 0;
    if(*ppos > 0 || count < BUFSIZE){
        printk(KERN_WARNING LOG_PROC("FAILED *ppos > 0 || count < BUFSIZE"));
        return 0;
    }
    int i = 0;
    for(i; i<history_size; i++){
        len += sprintf(buf+len,"%d\n",get_from_history(i));
    }
    printk(KERN_WARNING "Generated result \n %s", buf);
    *ppos = len;
    return len;
}

static struct file_operations dev_fops = {
        .owner = THIS_MODULE,
        .open = dev_open,
        .release = dev_close,
        .read = dev_read,
        .write = dev_write,
};


static int init_entity(size_t i) {
    char var[64] = VARIANT_NAME "_";
    char number_string[32];
    sprintf(number_string, "%d", i);
    strcat(var, number_string);
    
    if (alloc_chrdev_region(&my_dev[i], DEV_FIRST_MAJOR, ENTITIES, var) < 0) {
        printk(KERN_INFO LOG_MODULE("Failed to alloc character drive region"));
        return -1;
    }

    char_drv_class[i] = class_create(THIS_MODULE, var);
    if (!char_drv_class[i]) {
        printk(KERN_INFO LOG_MODULE("Failed to create character device class"));
        unregister_chrdev_region(my_dev[i], ENTITIES);
        return -1;
    }

    if (!device_create(char_drv_class[i], NULL, my_dev[i], NULL, var)) {
        printk(KERN_INFO LOG_MODULE("Failed to create device"));
        class_destroy(char_drv_class[i]);
        unregister_chrdev_region(my_dev[i], ENTITIES);
        return -1;
    }

    cdev_init(&char_dev, &dev_fops);
    if (cdev_add(&char_dev, my_dev[i], 1) < 0) {
        printk(KERN_INFO LOG_MODULE("Failed to add character device"));
        device_destroy(char_drv_class[i], my_dev[i]);
        class_destroy(char_drv_class[i]);
        unregister_chrdev_region(my_dev[i], ENTITIES);
        return -1;
    }
    return 0;
}

static int simple_init(void) {
    printk(KERN_ALERT LOG_MODULE("Initializing module \n"));
    ent = proc_create(VARIANT_NAME, 0777, NULL, &myops);
    if (!ent) {
        printk(KERN_INFO LOG_MODULE("Failed to create proc dir entry"));
        return -1;
    }
    size_t i = 0;
    for (; i < ENTITIES; ++i) {
        if (init_entity(i) < 0) {
            proc_remove(ent);
            return -1;
        }
    }
    printk(KERN_INFO LOG_MODULE("Sucessfully initialized"));

    return 0;
}

static void simple_cleanup(void) {
    cdev_del(&char_dev);
    size_t i = 0;
    for (; i < ENTITIES; ++i) {
        device_destroy(char_drv_class[i], my_dev[i]);
        class_destroy(char_drv_class[i]);
        unregister_chrdev_region(my_dev[i], ENTITIES);
    }
    proc_remove(ent);
    printk(KERN_INFO LOG_MODULE("Sucessfully exit"));
}

module_init(simple_init);
module_exit(simple_cleanup);